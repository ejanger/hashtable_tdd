﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collection
{
    public class HashTable<T,V>
    {
        protected struct Element
        {
            public T key;
            public V value;
        }

        public int Count { get; protected set; }

        public List<T> Keys { 
            get
        {
            return GetKeys();
        } }

        public List<V> Values
        {
            get
            {
                return GetValues();
            }
        }

        public V this[T key]
        {
            get
            {
                return Get(key);
            }
            set
            {
                Add(key, value);
            }
        }

        protected List<Element>[] table;
        protected int size;

        public HashTable()
        {
            Count = 0;
            size = 16;
            table = new List<Element>[size];
            Array.Clear(table, 0, size);
        }

        protected int GetTableInd(T key)
        {
            return Math.Abs(key.GetHashCode()) % size;
        }

        public void Add(T key, V value)
        {
            int index = GetTableInd(key);
            Element newElem;
            newElem.key = key;
            newElem.value = value;
            if (table[index] != null)
            {
                if (table[index].Any<Element>(x => key.Equals(x.key)))
                    throw new ArgumentException("Dublicate key");
                table[index].Add(newElem);
            }
            else
                table[index] = new List<Element>(new Element[] { newElem });
            Count++;
        }

        public V Get(T key)
        {
            int index = GetTableInd(key);
            if (table[index] == null ||
                table[index].Count <= 0)
                throw new ArgumentException("Not contains key");
            for (int i = 0; i < table[index].Count; i++)
            {
                if (key.Equals(table[index][i].key))
                {
                    V value = table[index][i].value;
                    return value;
                }
            }
            throw new ArgumentException("Not contains key");
        }

        public V Remove(T key)
        {
            int index = GetTableInd(key);
            if (table[index] == null ||
                table[index].Count <=0)
                throw new ArgumentException("Not contains key");
            for (int i=0; i<table[index].Count; i++)
            {
                if (key.Equals(table[index][i].key))
                {
                    V value = table[index][i].value;
                    table[index].RemoveAt(i);
                    Count--;
                    return value;
                }
            }
            throw new ArgumentException("Not contains key");
        }

        public bool ContainsKey(T key)
        {

            int index = GetTableInd(key);
            if (table[index] == null ||
                table[index].Count <= 0)
                return false;
            for (int i = 0; i < table[index].Count; i++)
            {
                if (key.Equals(table[index][i].key))
                {
                    return true;
                }
            }
            return false;
        }

        public List<T> GetKeys()
        {
 	        List<T> keys = new List<T>(Count);
            for (int index=0; index<table.Length; index++)
            {
                if (table[index] == null)
                    continue;
                for (int i = 0; i < table[index].Count; i++)
                {
                    keys.Add(table[index][i].key);
                }
            }
            return keys;
        }

        public List<V> GetValues()
        {
            List<V> values = new List<V>(Count);
            for (int index = 0; index < table.Length; index++)
            {
                if (table[index] == null)
                    continue;
                for (int i = 0; i < table[index].Count; i++)
                {
                    values.Add(table[index][i].value);
                }
            }
            return values;
        }
    }
}
