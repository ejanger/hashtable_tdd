﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Collection;
using System.Collections.Generic;

namespace HashTableTest
{
    [TestClass]
    public class HashTableTest
    {
        [TestMethod]
        public void Add_IntKeyIntVal()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
        }

        [TestMethod]
        public void AddGet_IntKeyIntVal()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
            int retVal = ht.Get(key);
            Assert.AreEqual(value, retVal);
        }

        [TestMethod]
        public void AddRemove_IntKeyIntVal()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
            int retVal = ht.Remove(key);
            Assert.AreEqual(value, retVal);
            Assert.AreEqual(0, ht.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void Add2KeyWithCollision_IntKeyIntVal_Exception()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
            ht.Add(key, value);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void RemoveNotExistKey_Exception()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            ht.Remove(0);
        }

        [TestMethod]
        public void AddGetRemoveAndAddSameKey()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value =987;
            for (int i = 0; i < 2; i++)
            {
                ht.Add(key, value);
                Assert.AreEqual(1, ht.Count);
                Assert.AreEqual(value, ht.Get(key));
                ht.Remove(key);
                Assert.AreEqual(0, ht.Count);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void GetNotExistKey()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            ht.Get(0);
        }

        [TestMethod]
        public void ContainsKey()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
            Assert.IsTrue(ht.ContainsKey(key));
            int key2 = 555, value2 = 777;
            Assert.IsFalse(ht.ContainsKey(key2));
            ht.Add(key2, value2);
            Assert.AreEqual(2, ht.Count);
            Assert.IsTrue(ht.ContainsKey(key2));
            ht.Remove(key);
            Assert.AreEqual(1, ht.Count);
            Assert.IsFalse(ht.ContainsKey(key));
            Assert.IsTrue(ht.ContainsKey(key2));
            ht.Remove(key2);
            Assert.AreEqual(0, ht.Count);
            Assert.IsFalse(ht.ContainsKey(key2));
        }

        [TestMethod]
        public void Generic()
        {
            HashTable<MyObject, object> ht = new HashTable<MyObject, object>();
            object objValue = new object();
            MyObject objKey = new MyObject(123);
            ht.Add(objKey, objValue);
            Assert.IsTrue(ht.ContainsKey(objKey));
            Assert.AreEqual(objValue, ht.Get(objKey));
        }

        [TestMethod]
        public void AddWithSameHashCode()
        {
            var ht = new HashTable<MyObjectSameHashCode, int>();
            MyObjectSameHashCode key1 = new MyObjectSameHashCode(1, 1);
            MyObjectSameHashCode key2 = new MyObjectSameHashCode(2, 1);
            ht.Add(key1, 123);
            Assert.IsTrue(ht.ContainsKey(key1));
            Assert.IsFalse(ht.ContainsKey(key2));
            ht.Add(key2, 456);
            Assert.IsTrue(ht.ContainsKey(key2));
            Assert.AreEqual(123, ht.Remove(key1));
            Assert.IsTrue(ht.ContainsKey(key2));
        }

        [TestMethod]
        public void Keys()
        {
            var ht = new HashTable<int, int>();
            int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Array.ForEach<int>(keys, x => ht.Add(x, 123));
            List<int> copyKeys = ht.Keys;
            copyKeys.Sort();
            for (int i = 0; i < keys.Length; i++)
                Assert.AreEqual(keys[i], copyKeys[i]);
        }

        [TestMethod]
        public void Values()
        {
            var ht = new HashTable<int, int>();
            int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Array.ForEach<int>(keys, x => ht.Add(x, 123));
            List<int> copyValues = ht.Values;
            for (int i = 0; i < keys.Length; i++)
                Assert.AreEqual(123, copyValues[i]);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void PropertyForGet()
        {
            HashTable<int, int> ht = new HashTable<int, int>();
            Assert.AreEqual(0, ht.Count);
            int key = 123, value = 999;
            ht.Add(key, value);
            Assert.AreEqual(1, ht.Count);
            int retVal = ht[key];
            Assert.AreEqual(value, retVal);
            int errorRet = ht[1];
        }

        private class MyObject
        {
            public int Value {get; protected set;}
            public MyObject(int val)
            {
                Value = val;
            }

            public override int GetHashCode()
            {
                return Value;
            }

            public override bool Equals(object obj)
            {
                MyObject another = obj as MyObject;
                if (another != null)
                {
                    return another.Value == this.Value;
                }
                return false;
            }
        }

        private class MyObjectSameHashCode:MyObject
        {
            public int HashCode { get; protected set; }

            public MyObjectSameHashCode(int val, int hashCode)
                :base(val)
            {
                HashCode = hashCode;
            }

            public override int GetHashCode()
            {
                return HashCode;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void AddAndGetNotExist_Exception()
        {
            var ht = new HashTable<MyObjectSameHashCode, int>();
            MyObjectSameHashCode key1 = new MyObjectSameHashCode(1, 1);
            MyObjectSameHashCode key2 = new MyObjectSameHashCode(2, 1);
            ht.Add(key1, 123);
            Assert.IsTrue(ht.ContainsKey(key1));
            Assert.IsFalse(ht.ContainsKey(key2));
            int retError = ht[key2];
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void AddAndRemoveNotExist_Exception()
        {
            var ht = new HashTable<MyObjectSameHashCode, int>();
            MyObjectSameHashCode key1 = new MyObjectSameHashCode(1, 1);
            MyObjectSameHashCode key2 = new MyObjectSameHashCode(2, 1);
            ht.Add(key1, 123);
            Assert.IsTrue(ht.ContainsKey(key1));
            Assert.IsFalse(ht.ContainsKey(key2));
            ht.Remove(key2);
        }

        [TestMethod]
        public void ContainsKey_NotContains()
        {
            var ht = new HashTable<MyObjectSameHashCode, int>();
            MyObjectSameHashCode key1 = new MyObjectSameHashCode(1, 1);
            Assert.IsFalse(ht.ContainsKey(key1));
        }
    }
}
